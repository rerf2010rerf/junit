package nsu.javacourse.junit;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import nsu.javacourse.junit.annotations.After;
import nsu.javacourse.junit.annotations.Before;
import nsu.javacourse.junit.annotations.Test;

public class TestExecutor {

  private List<Method> befores;
  private List<Method> afters;
  private final Class<?> testClass;
  private ClassTestResult classTestResult;

  public TestExecutor(final String className) {
    this.testClass = loadClass(className);

    if (null != testClass) {
      final Method[] methods = testClass.getMethods();
      this.befores = findMethodsByAnnotation(methods, Before.class);
      this.afters = findMethodsByAnnotation(methods, After.class);
    }
  }

  public ClassTestResult runTest() {
    if (null != classTestResult && ClassTestResultCode.FAIL_LOADING_CLASS.equals(classTestResult.getResultCode())) {
      return classTestResult;
    }

    final Method[] methods = testClass.getMethods();
    final List<TestEntry> entries = createTestEntries(methods);

    final Object testObject;
    try {
      testObject = testClass.newInstance();
      classTestResult = new ClassTestResult(testClass.getName(), ClassTestResultCode.SUCCEEDED);
    } catch (Throwable ex) {
      return new ClassTestResult(testClass.getName(), ClassTestResultCode.FAIL_WITH_INSTANTIATION_EXCEPTION, ex);
    }

    for (final TestEntry entry : entries) {
      classTestResult.addMethodResult(executeTestEntry(testObject, entry));
    }

    return classTestResult;
  }

  private Class<?> loadClass(final String className) {
    try {
      return Class.forName(className);
    } catch (ClassNotFoundException ex) {
      classTestResult = new ClassTestResult(className, ClassTestResultCode.FAIL_LOADING_CLASS, ex);
      return null;
    }
  }

  private MethodTestResult executeTestEntry(final Object testObject, final TestEntry entry) {
    final String testHeader = entry.getMethod().getName();

    try {
      for (final Method before : befores) {
        before.invoke(testObject);
      }
    } catch (Throwable ex) {
      return new MethodTestResult(testHeader, MethodTestResultCode.FAIL_WITH_BEFORE_METHOD_EXCEPTION, ex);
    }

    MethodTestResult succeededResult = null;
    try {
      entry.getMethod().invoke(testObject);
      if (entry.needExpectedException()) {
        return new MethodTestResult(testHeader, MethodTestResultCode.FAIL_NO_EXPECTED_EXCEPTION);
      }
    } catch (InvocationTargetException methodException) {
      final Throwable target = methodException.getTargetException();
      if (!entry.needExpectedException()) {
        return new MethodTestResult(testHeader, MethodTestResultCode.FAIL_WITH_EXCEPTION,
          methodException.getTargetException());
      }
      if (entry.needExpectedException() && !entry.getExpectedException().isInstance(target)) {
        return new MethodTestResult(testHeader, MethodTestResultCode.FAIL_NO_EXPECTED_EXCEPTION,
          methodException.getTargetException());
      } else {
        succeededResult = new MethodTestResult(testHeader, MethodTestResultCode.SUCCEEDED_WITH_EXPECTED_EXCEPTION,
          methodException.getTargetException());
      }
    } catch (Throwable ex) {
      return new MethodTestResult(testHeader, MethodTestResultCode.FAIL_WITH_EXCEPTION, ex);
    }

    try {
      for (final Method after : afters) {
        after.invoke(testObject);
      }
    } catch (Throwable ex) {
      return new MethodTestResult(testHeader, MethodTestResultCode.FAIL_WITH_AFTER_METHOD_EXCEPTION, ex);
    }

    if (null == succeededResult) {
      return new MethodTestResult(testHeader, MethodTestResultCode.SUCCEEDED_NO_EXCEPTIONS);
    } else {
      return succeededResult;
    }
  }

  private List<Method> findMethodsByAnnotation(final Method[] methods, final Class<? extends Annotation> annotation) {
    return Stream.of(methods).filter(method -> method.getAnnotationsByType(annotation).length > 0)
      .collect(Collectors.toList());
  }

  private List<TestEntry> createTestEntries(final Method[] methods) {
    return Stream.of(methods)
      .filter(method -> null != method.getAnnotation(Test.class))
      .map(method -> {
        final Test testAnnot = method.getAnnotation(Test.class);
        return new TestEntry(method, testAnnot.ExpectedException());
      }).collect(Collectors.toList());
  }

  private static class TestEntry {
    private final Method method;
    private final Class<? extends Throwable> expectedException;

    public TestEntry(final Method method, final Class<? extends Throwable> expectedException) {
      this.method = method;
      this.expectedException = expectedException;
    }

    public TestEntry(final Method method) {
      this(method, null);
    }

    public boolean needExpectedException() {
      return Test.None.class != expectedException;
    }

    public final Method getMethod() {
      return method;
    }

    public final Class<? extends Throwable> getExpectedException() {
      return expectedException;
    }

  }

}
