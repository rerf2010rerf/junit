package nsu.javacourse.junit;


public enum ClassTestResultCode {
  SUCCEEDED,
  FAIL_WITH_INSTANTIATION_EXCEPTION,
  FAIL_LOADING_CLASS
}
