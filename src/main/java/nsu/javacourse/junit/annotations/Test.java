package nsu.javacourse.junit.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Test {
	Class<? extends Throwable> ExpectedException() default None.class;

	static final class None extends Throwable {
		private None() {}
	}
}
