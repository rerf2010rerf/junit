package nsu.javacourse.junit;


public class Assertions {

  private Assertions(){}

  public static void assertTrue(final boolean cond) {
    if (!cond) {
      throw new TestAssertionError("Expected true, but actual value is false");
    }
  }
}
