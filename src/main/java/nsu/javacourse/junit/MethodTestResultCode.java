package nsu.javacourse.junit;



public enum MethodTestResultCode {
  SUCCEEDED_NO_EXCEPTIONS,
  SUCCEEDED_WITH_EXPECTED_EXCEPTION,
  FAIL_WITH_EXCEPTION,
  FAIL_NO_EXPECTED_EXCEPTION,
  FAIL_WITH_BEFORE_METHOD_EXCEPTION,
  FAIL_WITH_AFTER_METHOD_EXCEPTION;


  public static boolean isSucceeded(final MethodTestResultCode result) {
    return SUCCEEDED_NO_EXCEPTIONS.equals(result) || SUCCEEDED_WITH_EXPECTED_EXCEPTION.equals(result);
  }
}
