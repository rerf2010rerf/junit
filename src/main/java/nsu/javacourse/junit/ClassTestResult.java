package nsu.javacourse.junit;

import java.util.ArrayList;
import java.util.List;

public class ClassTestResult {
	private String testClassName;

	private ClassTestResultCode resultCode;
	
	private Throwable failCause;
	
	private List<MethodTestResult> methodResults = new ArrayList<>();

	public ClassTestResult(final String testClassName, final ClassTestResultCode resultCode) {
		this(testClassName, resultCode, null);
	}

	public ClassTestResult(final String testClassName, final ClassTestResultCode resultCode, final Throwable failCause) {
		this.testClassName = testClassName;
		this.resultCode = resultCode;
		this.failCause = failCause;
	}


	public void addMethodResult(final MethodTestResult methodResult) {
		methodResults.add(methodResult);
	}
	
	public final List<MethodTestResult> getMethodResults() {
		return methodResults;
	}
	
	public final String getTestClassName() {
		return testClassName;
	}

	public final Throwable getFailCause() {
		return failCause;
	}

	public ClassTestResultCode getResultCode() {
		return resultCode;
	}
}
