package nsu.javacourse.junit;


public class MethodTestResult {
	private String testMethodName;

	private MethodTestResultCode resultCode;

	private Throwable resultException;


	public MethodTestResult(final String testHeader, final MethodTestResultCode resultCode) {
		this(testHeader, resultCode, null);
	}

	public MethodTestResult(final String testHeader, final MethodTestResultCode resultCode, final Throwable resultException) {
		this.testMethodName = testHeader;
		this.resultCode = resultCode;
		this.resultException = resultException;
	}



	public final String getTestMethodName() {
		return testMethodName;
	}

	public final MethodTestResultCode getResultCode() {
		return resultCode;
	}

	public final Throwable getResultException() {
		return resultException;
	}
}