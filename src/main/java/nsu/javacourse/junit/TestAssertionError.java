package nsu.javacourse.junit;



public class TestAssertionError extends RuntimeException {

  public TestAssertionError(final String message) {
    super(message);
  }
}
