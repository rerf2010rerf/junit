package nsu.javacourse.junit;

import java.io.PrintStream;
import java.util.List;
import java.util.concurrent.*;

public class ParallelTestWorker {

  private final List<String> classes;
  private final TestResultPrinter resultPrinter;

  private final ExecutorService testExecutors;
  private final BlockingQueue<ClassTestResult> resultQueue;

  public ParallelTestWorker(final int threadCount, final PrintStream resultStream, final List<String> classes) {
    this.resultPrinter = new TestResultPrinter(resultStream);
    this.classes = classes;

    this.testExecutors = Executors.newFixedThreadPool(threadCount);
    resultQueue = new ArrayBlockingQueue<>(classes.size());
  }

  public void runTests() {

    final Thread printerThread = new Thread(new Runnable() {
      @Override
      public void run() {
        while (!Thread.currentThread().isInterrupted()) {
          try {
            final ClassTestResult result = resultQueue.take();
            resultPrinter.printResult(result);
          } catch (InterruptedException e) {
            printAfterInterruption();
          }
        }
        printAfterInterruption();
      }

      private void printAfterInterruption() {
        Thread.currentThread().interrupt();
        ClassTestResult result;
        while ((result = resultQueue.poll()) != null) {
          resultPrinter.printResult(result);
        }
      }
    });
    printerThread.start();

    for (final String className : classes) {
      testExecutors.submit(() -> {
        final TestExecutor testExecutor = new TestExecutor(className);
        try {
          resultQueue.put(testExecutor.runTest());
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      });
    }
    testExecutors.shutdown();
    try {
      testExecutors.awaitTermination(Long.MAX_VALUE, TimeUnit.DAYS);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    printerThread.interrupt();
  }

}
