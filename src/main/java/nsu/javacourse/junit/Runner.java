package nsu.javacourse.junit;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Runner {
	
	public static void main(final String[] args) throws ClassNotFoundException {
		if (args.length < 2) {
			printError();
			return;
		}
		final int threadCount;
		try {
			threadCount = Integer.valueOf(args[0]);
		} catch (NumberFormatException ex) {
			printError();
			return;
		}

		final ParallelTestWorker testWorker = new ParallelTestWorker(threadCount,
			System.out, Stream.of(args).skip(1).collect(Collectors.toList()));

		testWorker.runTests();
	}


	private static void printError() {
		System.out.println("Введите первым аргументом необходимое количество потоков-тестеров, а во всех" +
			"последующих - полные имена тестируемых классов");
	}

}
