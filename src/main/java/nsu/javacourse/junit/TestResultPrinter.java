package nsu.javacourse.junit;

import java.io.PrintStream;


public class TestResultPrinter {

  private final PrintStream resultStream;

  public TestResultPrinter(PrintStream resultStream) {
    this.resultStream = resultStream;
  }

  public void printResult(final ClassTestResult result) {
    resultStream.println("\n============================================");
    resultStream.printf("Test class: %s\n", result.getTestClassName());

    if (!ClassTestResultCode.SUCCEEDED.equals(result.getResultCode())) {
      resultStream.printf("Error test class %s:\n", result.getTestClassName());
      if (null != result.getFailCause()) {
        resultStream.println(result.getFailCause().getMessage());
        result.getFailCause().printStackTrace(resultStream);
      }
    } else {
      result.getMethodResults().forEach(methodResult -> {
        resultStream.printf("\nTest method: %s\n", methodResult.getTestMethodName());

        if (MethodTestResultCode.isSucceeded(methodResult.getResultCode())) {
          resultStream.printf("\tTest succeeded: %s\n", methodResult.getResultCode());
        } else {
          resultStream.printf("\tTest fail: failCode = %s\n", methodResult.getResultCode());
          if (null != methodResult.getResultException()) {
            resultStream.println(methodResult.getResultException().getMessage());
            methodResult.getResultException().printStackTrace(resultStream);
          }
        }

      });
    }
  }
}
